# Cassini

## Installation

Requires the installation of graphviz on your terminal or command line beforehand.
Particularly the dot command packaged with graphviz is used to generate the png.

    - Installation on MacOS is `brew install graphviz`
    - Installation on Ubuntu is `apt-get install graphviz`


If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `cassini` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:cassini, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/cassini](https://hexdocs.pm/cassini).

## Usage

Essentially the GraphGenerate module has one main function called graph_generate.

The graph_generate function takes 5 parameters.

1. The first parameter is the lib folder of your elixir project.
2. The second parameter is optional and false by default, it is flag for png generation
3. The third parameter is optional and false by default, it is flag for whether you 
<br>would like your graph clustered according to parent modules.
4. The fourth parameter is the width of the graph specified as 0.5 by default.
5. The fifth parameter is the height of the graph specified as 7 by default.

Example Usage:
    
    Cassini.GraphGenerate.graph_generate("lib", true, true, 1, 10)

