.PHONY: setup clean deps test run help


deps: ## Upgrades depenencies
	@mix do deps

test: ## Run test cases 
	@MIX_ENV=test mix do test 

run: setup ## Start a dev instance
	@MIX_ENV=dev iex -S mix 

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
