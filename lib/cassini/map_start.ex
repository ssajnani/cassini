defmodule Cassini.MapStart do
  def list_all(filepath) do
    _list_all(filepath)
  end

  defp _list_all(filepath) do
    cond do
      String.contains?(filepath, ".git") -> []
      true -> expand(File.ls(filepath), filepath)
    end
  end

  defp expand({:ok, files}, path) do
    files
    |> Enum.flat_map(&_list_all("#{path}/#{&1}"))
  end

  defp get_module(klist, as_relation) when is_list(klist) do
    if Keyword.keyword?(klist) do
      get_module(klist[:do], as_relation)
    else
      [head | tail] = klist

      {head_val, tail_val} =
        case head do
          {:alias, _, [{{_, _, [{:__aliases__, _, top_level}, _]}, _, lower_level}]} ->
            {head_val, as_relation} =
              Enum.reduce(lower_level, {[], as_relation}, fn {:__aliases__, _, lower_name},
                                                             {head_val, as_relation} ->
                {[top_level ++ lower_name | head_val],
                 Map.put(as_relation, lower_name, top_level ++ lower_name)}
              end)

            {[{:used_modules, head_val}], get_module(tail, as_relation)}

          {:alias, _, [{:__aliases__, _, names}, [as: {:__aliases__, _, aliases}]]} ->
            as_relation = Map.put(as_relation, aliases, names)
            {get_module(head, as_relation), get_module(tail, as_relation)}

          _ ->
            {get_module(head, as_relation), get_module(tail, as_relation)}
        end

      case !!head_val do
        true ->
          case !!tail_val do
            true ->
              Keyword.merge(head_val, tail_val, fn _k, k1, k2 ->
                k1 ++ k2
              end)

            false ->
              head_val
          end

        false ->
          tail_val
      end
    end
  end

  defp get_module({:defmodule, _, third}, as_relation) do
    aliases = Enum.at(third, 0)
    {:__aliases__, _, module_type} = aliases
    [{:defmodule, [module_type]}] ++ get_module(third, as_relation)
  end

  defp get_module({:__aliases__, _, third}, as_relationship) do
    case Code.ensure_compiled?(Code.eval_quoted({:__aliases__, [alias: false], third}) |> elem(0)) do
      true ->
        case Map.get(as_relationship, third, nil) do
          nil -> [{:used_modules, [third]}]
          _ -> [{:used_modules, [as_relationship[third]]}]
        end

      _ ->
        []
    end
  end

  defp get_module({_, _, third}, _) when is_bitstring(third) do
    []
  end

  defp get_module({atom, _, third}, as_relationship) do
    case atom == :as do
      true ->
        []

      false ->
        get_module(third, as_relationship)
    end
  end

  defp get_module(_, _) do
    []
  end

  defp expand({:error, _}, path) do
    case String.contains?(path, [".ex", ".exs"]) do
      true ->
        [
          {String.to_atom(path),
           get_module(elem(Code.string_to_quoted(elem(File.read(path), 1)), 1), %{})}
        ]

      false ->
        []
    end
  end
end
