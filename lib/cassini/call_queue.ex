defmodule Cassini.CallQueue do
  @moduledoc """
  FIFO queue implemented with ETS Ordered Sets.
  """
  defstruct [
    :length,
    :tid
  ]

  @type t :: %__MODULE__{
          length: integer() | nil,
          tid: any
        }

  def new(length) when is_integer(length) do
    tid = :ets.new(__MODULE__, [:public, :ordered_set])
    :ets.insert(tid, {-1, 0})
    :ets.insert(tid, {-2, -1})
    %__MODULE__{tid: tid, length: length}
  end

  def new() do
    tid = :ets.new(__MODULE__, [:public, :ordered_set])
    %__MODULE__{tid: tid}
  end

  def put(%__MODULE__{tid: tid, length: length}, item) do
    [{_key, pointer_to_first}] = :ets.lookup(tid, -1)
    [{_key, pointer_to_last}] = :ets.lookup(tid, -2)
    next_first = rem(pointer_to_first + 1, length)
    next_last = rem(pointer_to_last + 1, length)

    case pointer_to_last == -1 do
      true ->
        :ets.insert(tid, {pointer_to_first, item})
        :ets.insert(tid, {-2, pointer_to_first})
        :ok

      false ->
        [{_key, return_item}] = :ets.lookup(tid, pointer_to_first)
        :ets.insert(tid, {next_last, item})
        :ets.insert(tid, {-2, next_last})

        case next_last == pointer_to_first do
          true ->
            :ets.insert(tid, {-1, next_first})
            return_item

          false ->
            :ok
        end
    end
  end

  def take(%__MODULE__{tid: tid, length: length}) do
    [{_key, pointer_to_first}] = :ets.lookup(tid, -1)
    [{_key, pointer_to_last}] = :ets.lookup(tid, -2)
    next_first = rem(pointer_to_first + 1, length)
    next_last = rem(pointer_to_last + 1, length)

    case pointer_to_last == -1 do
      true ->
        :ok

      false ->
        [{_key, return_val}] = :ets.lookup(tid, pointer_to_first)
        :ets.delete(tid, pointer_to_first)
        :ets.insert(tid, {-1, next_first})

        case next_first == next_last do
          true ->
            :ets.insert(tid, {-2, -1})

          false ->
            nil
        end

        return_val
    end
  end

  def length(%__MODULE__{tid: tid}), do: :ets.info(tid, :size)

  def show_all(%__MODULE__{tid: tid}), do: :ets.tab2list(tid)

  def terminate(%__MODULE__{tid: tid}) do
    true = :ets.delete(tid)
    :ok
  end
end
