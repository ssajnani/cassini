defmodule Cassini.Tracer do
  use GenServer
  alias Cassini.GraphGenerate, as: GG
  alias Cassini.TraceGraphs, as: TG
  alias Cassini.CallQueue, as: CQ

  @total_iters Application.get_env(:cassini, :total_number_of_counted_iterations, 10000)

  def start_link(start) do
    modules = TG.get_module_instantiations()

    GenServer.start_link(__MODULE__, {start, modules}, name: __MODULE__)
  end

  def init({start, modules}) do
    case start do
      [] ->
        :erlang.trace(:all, true, [:call])

        for module <- modules do
          module.__info__(:functions)
          # {:message, {:caller}}]}], [:local])
          :erlang.trace_pattern({module, :_, :_}, [{:_, [], [{:message, {:caller}}]}], [:local])
        end

      _ ->
        nil
    end

    schedule_graph_generation()
    :ets.new(:edge_map, [:named_table])
    :ets.new(:edge_ratios, [:named_table])
    :ets.new(:modules, [:named_table])

    modules
    |> Enum.map(fn mod ->
      mod = mod |> to_string
      :ets.insert(:modules, {mod, self()})
    end)

    {:ok,
     [
       0,
       CQ.new(@total_iters)
     ]}
  end

  def handle_info({:trace, _, :call, {mod, _, _}, {:gen_server, _, _}}, state) do
    {:noreply, state}
  end

  def handle_info({:trace, _, :call, {mod, _, _}, {from, _, _}}, state) do
    [total_count, queue] = state
    from = from |> to_string
    mod = mod |> to_string

    case :ets.lookup(:modules, from) != [] and :ets.lookup(:modules, mod) != [] and from != mod do
      true ->
        from = from |> String.replace_prefix("Elixir.", "")
        mod = mod |> String.replace_prefix("Elixir.", "")

        total_count =
          case add_new_item(from, mod, queue) do
            :ok ->
              total_count + 1

            item ->
              :ets.update_counter(:edge_map, item, -1, {1, 0})
              total_count
          end

        {:noreply, [total_count, queue]}

      false ->
        {:noreply, [total_count, queue]}
    end
  end

  def add_new_item(from, mod, queue) do
    :ets.update_counter(:edge_map, {from, mod}, 1, {1, 0})
    CQ.put(queue, {from, mod})
  end

  def get_state() do
    GenServer.call(__MODULE__, :get_state)
  end

  def handle_call(:get_state, _from, [total, _] = state) do
    convert_state_ratios(total)
    {:reply, :ets.tab2list(:edge_ratios), state}
  end

  def handle_info(:generate_graph, [total, _] = state) do
    convert_state_ratios(total)

    GG.graph_generate(
      Application.get_env(:cassini, :dir, "lib"),
      Application.get_env(:cassini, :gen_png, true),
      Application.get_env(:cassini, :group, true),
      Application.get_env(:cassini, :width, 0.5),
      Application.get_env(:cassini, :length, 7),
      Application.get_env(:cassini, :ignore, [])
    )

    schedule_graph_generation()

    {:noreply, state}
  end

  defp schedule_graph_generation() do
    Process.send_after(
      __MODULE__,
      :generate_graph,
      Application.get_env(:cassini, :frequency, 300 * 1000)
    )
  end

  def convert_state_ratios(total) do
    :ets.delete_all_objects(:edge_ratios)
    edge_totals = :ets.tab2list(:edge_map)

    {max, min} =
      Enum.reduce(edge_totals, {0, 1}, fn {mods, val}, {max, min} ->
        ret = val / total

        max =
          case ret > max,
            do:
              (
                true ->
                  ret

                false ->
                  max
              )

        min =
          case ret < min,
            do:
              (
                true ->
                  ret

                false ->
                  min
              )

        :ets.insert(:edge_ratios, {mods, ret})

        {max, min}
      end)

    :ets.insert(:edge_ratios, {"max", max})
    :ets.insert(:edge_ratios, {"min", min})
  end
end
