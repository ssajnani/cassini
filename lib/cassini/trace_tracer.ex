defmodule Cassini.TraceTracer do
  use GenServer
  alias Cassini.TraceGraphs, as: TG

  def start_link(_args) do
    modules = TG.get_module_instantiations()

    GenServer.start_link(__MODULE__, modules, name: __MODULE__)
  end

  def init(modules) do
    :erlang.trace(:all, true, [:call])

    for module <- modules do
      module.__info__(:functions)
      :erlang.trace_pattern({module, :_, :_}, [{:_, [], [{:message, {:caller}}]}], [:local])
    end

    {:ok, :ok}
  end

  def handle_info({:trace, _, :call, {mod, _, _}, {:gen_server, _, _}}, state) do
    {:noreply, state}
  end

  def handle_info(
        {:trace, _, :call, {Cassini.Tracer, _, _}, {Cassini.Tracer, _, _}},
        state
      ) do
    {:noreply, state}
  end

  def handle_info(
        {:trace, _, :call, {Cassini.CallQueue, _, _}, {Cassini.CallQueue, _, _}},
        state
      ) do
    {:noreply, state}
  end

  def handle_info(
        {:trace, _, :call, {Cassini.CallQueue, _, _}, {Cassini.Tracer, _, _}},
        state
      ) do
    {:noreply, state}
  end

  def handle_info(values, state) do
    send(Cassini.Tracer, values)
    {:noreply, :ok}
  end
end
