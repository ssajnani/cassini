defmodule Cassini.GraphGenerate do
  alias Cassini.Graph
  alias Cassini.Subgraph
  alias Cassini.Utils.Randomizer
  alias Cassini.MapStart
  alias Cassini.TraceGraphs, as: TG

  @color_gradient [
    "#FF0000",
    "#FF1100",
    "#FF2300",
    "#FF3400",
    "#FF4600",
    "#FF5700",
    "#FF6900",
    "#FF7B00",
    "#FF8C00",
    "#FF9E00",
    "#FFAF00",
    "#FFC100",
    "#FFD300",
    "#FFE400",
    "#FFF600",
    "#F7FF00",
    "#E5FF00",
    "#D4FF00",
    "#C2FF00",
    "#B0FF00",
    "#9FFF00",
    "#8DFF00",
    "#7CFF00",
    "#6AFF00",
    "#58FF00",
    "#47FF00",
    "#35FF00",
    "#24FF00",
    "#12FF00",
    "#00FF00"
  ]

  def graph_generate(
        modules,
        compile_output \\ false,
        w_clusters \\ false,
        width \\ 0.5,
        height \\ 7,
        ignore_modules \\ []
      )

  def graph_generate(
        modules,
        compile_output,
        w_clusters,
        width,
        height,
        ignore_modules
      )
      when is_bitstring(modules) do
    modules =
      with modules <- TG.get_module_info() do
        modules
      else
        _ ->
          MapStart.list_all(modules)
      end

    graph_generate(modules, compile_output, w_clusters, width, height, ignore_modules)
  end

  def graph_generate(
        modules,
        compile_output,
        w_clusters,
        width,
        height,
        ignore_modules
      ) do
    %{graph: graph} =
      case TG.get_graph() do
        nil ->
          result =
            iter_modules(
              %{groups: %{}, edge_map: %{}, vertex_list: %{}, graph: Graph.new()},
              modules,
              ignore_modules
            )

          %{groups: groups, edge_map: edges, vertex_list: vertices, graph: graph} = result

          graph =
            case w_clusters do
              true ->
                clusters =
                  Enum.reduce(groups, [], fn {k, v}, acc ->
                    case v == %{} do
                      true -> acc
                      false -> [generate_clusters(graph, v, k) | acc]
                    end
                  end)

                %{graph | subgraphs: clusters}

              false ->
                graph
            end
            |> add_update_edges(edges, vertices, false)

          result = Map.put(result, :graph, graph)

          TG.set_graph(result)

          result

        %{graph: graph, edge_map: edges, vertex_list: vertices} = result ->
          result
          |> Map.put(:graph, graph |> add_update_edges(edges, vertices, true))

          TG.set_graph(result)
          result

        result ->
          result
      end

    random_name = "results/" <> Randomizer.randomizer(10)
    graph = Map.put(graph, :graph_properties, ranksep: height, nodesep: width)
    ret_val = "Results stored in " <> random_name <> ".dot"
    ret_val_png = ret_val <> " and " <> random_name <> ".png"

    compile_write_curry = fn function, graph, ret ->
      function.(graph, random_name)
      {:ok, ret}
    end

    {function_choice, return_val} =
      case compile_output do
        true ->
          {&Graph.compile/2, ret_val_png}

        false ->
          {&Graph.write/2, ret_val}
      end

    compile_write_curry.(function_choice, graph, return_val)
  end

  defp add_update_edges(graph, edges, vertices, update) do
    vertices_reversed = Enum.into(vertices, %{}, &{elem(&1, 1), elem(&1, 0)})

    vertex_edge_mapping = TG.get_edge_map()

    Enum.reduce(edges, graph, fn {from, to_mods}, graph_edge ->
      Enum.reduce(to_mods, graph_edge, fn to, graph_edge ->
        module_name = Map.get(vertices_reversed, from, "")
        to_full_name = Map.get(vertices_reversed, to, "")

        case vertices[module_name] == vertices[to_full_name] do
          false ->
            {graph_edge, _} =
              case update do
                true ->
                  case Kernel.get_in(vertex_edge_mapping, [from, to]) do
                    nil ->
                      {graph_edge, nil}

                    edge ->
                      Graph.add_edge(
                        graph_edge,
                        edge,
                        from,
                        to,
                        color: get_color_based_on_weight(module_name, to_full_name)
                      )
                  end

                false ->
                  Graph.add_edge(
                    graph_edge,
                    from,
                    to,
                    color: get_color_based_on_weight(module_name, to_full_name)
                  )
              end

            graph_edge

          _ ->
            graph_edge
        end
      end)
    end)
  end

  defp generate_clusters(graph, groups, label) do
    cluster =
      Enum.reduce(groups, [[], []], fn {k, v}, acc ->
        [head | [tail]] = acc

        case v == %{} do
          true ->
            [[k | head] | [tail]]

          false ->
            subgraph = generate_clusters(graph, v, k)
            [[head] | [[subgraph | tail]]]
        end
      end)

    [vertices | [subgraphs]] = cluster
    sub_id = Graph.get_and_increment_subgraph_id(graph)
    Subgraph.new(sub_id, vertices, true, [label: label], subgraphs)
  end

  defp iter_modules(
         %{groups: groups, edge_map: edge_map, vertex_list: vlist, graph: graph},
         [
           head | tail
         ],
         ignore
       ) do
    module_info = head |> elem(1)

    modules_curry = fn first ->
      iter_modules(
        first,
        tail,
        ignore
      )
    end

    deps_curry = fn graph, id, full_name, grouping_setup, used_modules, vertex ->
      iter_dependencies(
        %{
          groups: iter_groups(id, groups, grouping_setup),
          edge_map: edge_map,
          vertex_list:
            case vertex do
              true ->
                Map.put(vlist, full_name, id)

              false ->
                vlist
            end,
          graph: graph
        },
        full_name,
        full_name,
        used_modules,
        ignore
      )
    end

    case module_info do
      [] ->
        modules_curry.(%{groups: groups, edge_map: edge_map, vertex_list: vlist, graph: graph})

      _ ->
        [name_pcs | _] = Keyword.get_values(module_info, :defmodule)
        used_modules = Keyword.get(module_info, :used_modules)
        grouping_setup = List.last(name_pcs)
        full_name = grouping_setup |> Enum.join(".")
        module_name = extract_name(grouping_setup)
        mod_vid = Map.get(vlist, full_name, nil)

        case {mod_vid, full_name not in ignore} do
          {nil, true} ->
            {graph, vid} = Graph.add_vertex(graph, module_name)

            deps_curry.(graph, vid, full_name, grouping_setup, used_modules, true)
            |> modules_curry.()

          _ ->
            deps_curry.(
              graph,
              mod_vid,
              full_name,
              grouping_setup,
              used_modules,
              false
            )
            |> modules_curry.()
        end
    end
  end

  defp extract_name(list_name) do
    List.last(list_name) |> Atom.to_string()
  end

  defp iter_groups(_, groups, []) do
    groups
  end

  defp iter_groups(module_name, groups, [head | tail]) do
    group_val = Map.get(groups, head, nil)

    case group_val do
      nil ->
        case tail == [] do
          true -> Map.put(groups, module_name, %{})
          false -> Map.put(groups, head, iter_groups(module_name, %{}, tail))
        end

      _ ->
        Map.put(groups, head, iter_groups(module_name, group_val, tail))
    end
  end

  defp iter_modules(vlist_graph, [], _) do
    vlist_graph
  end

  defp iter_dependencies(graph, _, _, [], _) do
    graph
  end

  defp iter_dependencies(
         %{groups: groups, edge_map: edge_map, vertex_list: vlist, graph: graph},
         module_name,
         full_name,
         [
           head | tail
         ],
         ignore
       ) do
    to_module = List.last(head) |> Atom.to_string()
    to_full_name = head |> Enum.join(".")

    mod_vid = Map.get(vlist, to_full_name, nil)

    case {mod_vid, to_full_name not in ignore} do
      {nil, true} ->
        {graph_vertex, vid} = Graph.add_vertex(graph, to_module)

        result_list = Map.put(vlist, to_full_name, vid)

        case result_list[module_name] == result_list[to_full_name] or
               MapSet.member?(
                 Map.get(edge_map, result_list[module_name], MapSet.new([])),
                 result_list[to_full_name]
               ) do
          false ->
            edge_map =
              Map.put(
                edge_map,
                result_list[module_name],
                MapSet.put(
                  Map.get(
                    edge_map,
                    result_list[module_name],
                    MapSet.new([])
                  ),
                  result_list[to_full_name]
                )
              )

            iter_dependencies(
              %{
                groups: iter_groups(vid, groups, head),
                edge_map: edge_map,
                vertex_list: result_list,
                graph: graph_vertex
              },
              module_name,
              full_name,
              tail,
              ignore
            )

          true ->
            iter_dependencies(
              %{
                groups: iter_groups(mod_vid, groups, head),
                edge_map: edge_map,
                vertex_list: result_list,
                graph: graph_vertex
              },
              module_name,
              full_name,
              tail,
              ignore
            )
        end

      {_, true} ->
        case vlist[module_name] == vlist[to_full_name] or
               MapSet.member?(
                 Map.get(edge_map, vlist[module_name], MapSet.new([])),
                 vlist[to_full_name]
               ) do
          false ->
            edge_map =
              Map.put(
                edge_map,
                vlist[module_name],
                MapSet.put(
                  Map.get(edge_map, vlist[module_name], MapSet.new([])),
                  vlist[to_full_name]
                )
              )

            iter_dependencies(
              %{groups: groups, edge_map: edge_map, vertex_list: vlist, graph: graph},
              module_name,
              full_name,
              tail,
              ignore
            )

          true ->
            iter_dependencies(
              %{groups: groups, edge_map: edge_map, vertex_list: vlist, graph: graph},
              module_name,
              full_name,
              tail,
              ignore
            )
        end

      _ ->
        iter_dependencies(
          %{groups: groups, edge_map: edge_map, vertex_list: vlist, graph: graph},
          module_name,
          full_name,
          tail,
          ignore
        )
    end
  end

  def get_color_based_on_weight(from, to) do
    with result <- :ets.lookup(:edge_ratios, {from, to}) do
      case result do
        [] ->
          "black"

        [{{_, _}, ratio}] ->
          [{_, max}] = :ets.lookup(:edge_ratios, "max")
          [{_, min}] = :ets.lookup(:edge_ratios, "min")

          range = max - min
          ratio_point = ratio - min

          ratio_over_range =
            case range do
              range when range == 0 ->
                14

              _ ->
                Kernel.trunc(ratio_point / range * 30)
            end

          @color_gradient
          |> Enum.at(
            case ratio_over_range,
              do:
                (
                  30 -> 29
                  _ -> ratio_over_range
                )
          )
      end
    else
      _ ->
        "black"
    end
  end
end
