defmodule Cassini.TraceGraphs do
  alias Cassini.MapStart, as: MS
  alias Cassini.Graph
  use GenServer

  def start_link(_args) do
    all_module_info = MS.list_all(Application.get_env(:cassini, :dir, "lib"))

    GenServer.start_link(__MODULE__, {all_module_info, nil, nil}, name: __MODULE__)
  end

  def init(module_info) do
    {:ok, module_info}
  end

  def get_module_info do
    GenServer.call(__MODULE__, :module_info)
  end

  def get_graph do
    GenServer.call(__MODULE__, :get_graph)
  end

  def set_graph(graph) do
    GenServer.call(__MODULE__, {:set_graph, graph})
  end

  def get_edge_map() do
    GenServer.call(__MODULE__, :get_edge_map)
  end

  def get_module_instantiations do
    module_info = get_module_info()

    Enum.reduce(module_info, [], fn {_, module_info}, acc ->
      case module_info do
        [
          defmodule: [module_name_list],
          used_modules: _
        ] ->
          [Code.eval_quoted({:__aliases__, [alias: false], module_name_list}) |> elem(0) | acc]

        _ ->
          acc
      end
    end)
  end

  @impl true
  def handle_call(:get_edge_map, _from, {_, nil, nil} = state) do
    {:reply, %{}, state}
  end

  @impl true
  def handle_call(:get_edge_map, _from, {module_info, %{graph: graph} = graph_info, nil}) do
    edge_list = Graph.get_edges(graph)

    edge_map =
      Enum.reduce(edge_list, %{}, fn edge, acc ->
        {_, v1, v2, _} = Graph.get_edge(graph, edge)
        Map.put(acc, v1, Map.get(acc, v1, %{}) |> Map.put(v2, edge))
      end)

    {:reply, edge_map, {module_info, graph_info, edge_map}}
  end

  @impl true
  def handle_call(:get_edge_map, _from, {_, _, edge_map} = state) do
    {:reply, edge_map, state}
  end

  @impl true
  def handle_call(:module_info, _from, {module_info, _, _} = state) do
    {:reply, module_info, state}
  end

  @impl true
  def handle_call(:get_graph, _from, {_, graph, _} = state) do
    {:reply, graph, state}
  end

  @impl true
  def handle_call({:set_graph, graph}, _from, {module_info, _, _}) do
    {:reply, :ok, {module_info, graph, nil}}
  end
end
